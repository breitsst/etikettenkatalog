package ch.luks.etiketten.etikette.api;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.openntf.xpt.core.dss.DSSException;
import org.openntf.xpt.core.dss.SingleObjectStore;

import ch.luks.etiketten.etikette.Etikette;
import ch.luks.etiketten.etikette.EtiketteStorageService;

public class EtiketteSessionFacade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Comparator wird f�r den Sort ben�tigt
	private static final Comparator<Etikette> COMPARATOR = new Comparator<Etikette>() {
		public int compare(Etikette arg0, Etikette arg1) {
			return arg0.getWirkstoff().toLowerCase().compareTo(arg1.getWirkstoff().toLowerCase());
		}
	};

	private String searchVal = "";
	private boolean isSearch = false;
	private List<Etikette> ListReturn = null;

	// Neue Etikette erstellen
	public Etikette createEtikette() {
		return EtiketteStorageService.getInstance().createObject();
	}

	// Etikette speichern
	public boolean saveEtikette(Etikette etikette) {
		ListReturn = null;
		return EtiketteStorageService.getInstance().save(etikette);
	}
	
	public void refreshDS() {
		ListReturn = null;
	}

	// Etikette l�schen
	public boolean deleteEtikette(Etikette etikette) {
		try {
			return EtiketteStorageService.getInstance().hardDelete(etikette, true);
		} catch (DSSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	// Eine Etikette lesen
	public Etikette getEtiketteByID(String id) {
		return EtiketteStorageService.getInstance().getById(id);
	}

	// EtikettenListe lesen
	public List<Etikette> getEtikettenListe(String abteilung, String letter, String searchValue, boolean search) {
		if (abteilung != "" && letter != "") {
			ListReturn = getEtikettenByAbteilungLetter(abteilung, letter);
		} else if (abteilung != "") {
			ListReturn = getEtikettenByAbteilung(abteilung);
		} else if (letter != "") {
			ListReturn = getEtikettenByLetter(letter);
		} else {
			isSearch = search;
			if (isSearch && searchValue != "") {
				ListReturn = searchEtikettenListe(searchValue);
			} else {
				if (isSame("")) {
					return ListReturn;
				}
				ListReturn = EtiketteStorageService.getInstance().getAll("vwLetter");
				// Sort
				Collections.sort(ListReturn, COMPARATOR);
			}
		}

		return ListReturn;
	}

	// Etiketten nach Abteilung und 1. Buchstaben (letter) lesen
	private List<Etikette> getEtikettenByAbteilungLetter(String abteilung, String letter) {
		String key = abteilung + "_" + letter;
		key = key.toLowerCase();
		if (isSame(key)) {
			return ListReturn;
		}
		ListReturn = EtiketteStorageService.getInstance().getObjectsByForeignId(key, "vwAbteilungLetter");
		// Sort
		Collections.sort(ListReturn, COMPARATOR);
		return ListReturn;
	}

	// Etiketten nach 1. Buchstaben (letter) lesen
	private List<Etikette> getEtikettenByLetter(String letter) {
		if (isSame(letter)) {
			return ListReturn;
		}
		ListReturn = EtiketteStorageService.getInstance().getObjectsByForeignId(letter.toLowerCase(), "vwLetter");
		// Sort
		Collections.sort(ListReturn, COMPARATOR);
		return ListReturn;
	}

	// Etiketten nach Abteilung lesen
	private List<Etikette> getEtikettenByAbteilung(String abteilung) {
		if (isSame(abteilung)) {
			return ListReturn;
		}
		ListReturn = EtiketteStorageService.getInstance().getObjectsByForeignId(abteilung.toLowerCase(), "vwAbteilung");
		// Sort
		Collections.sort(ListReturn, COMPARATOR);
		return ListReturn;
	}

	// Etiketten suchen
	private List<Etikette> searchEtikettenListe(String search) {
		// �berpr�fen ob Suchbegriff derselbe und Suche bereits vorhanden
		if (isSame(search)) {
			return ListReturn;
		}

		// Zusammenbauen des SuchStrings
		String cleanSearch = search.replaceAll("\\s+", " ");
		List<String> parts = Arrays.asList(cleanSearch.split(" "));
		String fSearch = "";
		if (parts.size() > 0) {
			for (String s : parts) {
				String tmp = s.replaceAll("\\s+", "");
				if (!tmp.equals("")) {
					if (!fSearch.equals("")) {
						fSearch += " AND ";
					}
					fSearch += "*" + tmp + "*";
				}
			}
		}

		ListReturn = EtiketteStorageService.getInstance().searchInView(fSearch, "(SearchView)");
		// Sort
		Collections.sort(ListReturn, COMPARATOR);
		return ListReturn;
	}

	private boolean isSame(String check) {
		if (searchVal.equals(check) && ListReturn != null) {
			return true;
		}
		searchVal = check;
		return false;
	}

	public SingleObjectStore<Etikette> getSOS() {
		return new SingleObjectStore<Etikette>(EtiketteStorageService.getInstance());
	}
}
