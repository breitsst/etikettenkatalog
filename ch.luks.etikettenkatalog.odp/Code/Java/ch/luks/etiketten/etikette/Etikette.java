package ch.luks.etiketten.etikette;

import java.io.Serializable;
import java.util.List;
import org.openntf.xpt.core.dss.annotations.DominoEntity;
import org.openntf.xpt.core.dss.annotations.DominoStore;
import com.ibm.xsp.http.MimeMultipart;

@DominoStore(Form = "frmEtikette", PrimaryKeyField = "ID", PrimaryFieldClass = String.class, View = "vwID")
public class Etikette implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@DominoEntity(FieldName = "ID")
	private String m_ID;

	@DominoEntity(FieldName = "NrImport")
	private String m_NrImport;

	@DominoEntity(FieldName = "Wirkstoff")
	private String m_Wirkstoff;

	@DominoEntity(FieldName = "ArtikelNr")
	private String m_ArtikelNr;

	@DominoEntity(FieldName = "ArtikelNrAlt")
	private String m_ArtikelNrAlt;

	@DominoEntity(FieldName = "EtikettenTyp")
	private String m_EtikettenTyp;

	// RichText Field
	@DominoEntity(FieldName = "EtiketteBild")
	private MimeMultipart m_EtiketteBild;

	@DominoEntity(FieldName = "EtiketteBildAlt")
	private MimeMultipart m_EtiketteBildAlt;

	// MultiValue Field
	@DominoEntity(FieldName = "Abteilungen")
	private List<String> m_Abteilungen;


	public void addAbteilung(String abteilung) {
		m_Abteilungen.add(abteilung);
	}

	public void removeAbteilung(String abteilung) {
		m_Abteilungen.remove(abteilung);
	}


	// get set
	public String getID() {
		return m_ID;
	}

	public void setID(String id) {
		m_ID = id;
	}

	public String getWirkstoff() {
		return m_Wirkstoff;
	}

	public void setWirkstoff(String wirkstoff) {
		m_Wirkstoff = wirkstoff;
	}

	public String getArtikelNr() {
		return m_ArtikelNr;
	}

	public void setArtikelNr(String artikelNr) {
		m_ArtikelNr = artikelNr;
	}

	public String getArtikelNrAlt() {
		return m_ArtikelNrAlt;
	}

	public void setArtikelNrAlt(String artikelNrAlt) {
		m_ArtikelNrAlt = artikelNrAlt;
	}

	public String getEtikettenTyp() {
		return m_EtikettenTyp;
	}

	public void setEtikettenTyp(String etikettenTyp) {
		m_EtikettenTyp = etikettenTyp;
	}

	public MimeMultipart getEtiketteBild() {
		return m_EtiketteBild;
	}

	public void setEtiketteBild(MimeMultipart etiketteBild) {
		m_EtiketteBild = etiketteBild;
	}

	public MimeMultipart getEtiketteBildAlt() {
		return m_EtiketteBildAlt;
	}

	public void setEtiketteBildAlt(MimeMultipart etiketteBildAlt) {
		m_EtiketteBildAlt = etiketteBildAlt;
	}

	public List<String> getAbteilungen() {
		return m_Abteilungen;
	}

	public void setAbteilungen(List<String> abteilungen) {
		m_Abteilungen = abteilungen;
	}

	public void setNrImport(String nrImport) {
		m_NrImport = nrImport;
	}

	public String getNrImport() {
		return m_NrImport;
	}
}
