package ch.luks.etiketten.etikette;

import java.util.UUID;
import org.openntf.xpt.core.dss.AbstractStorageService;

public class EtiketteStorageService extends AbstractStorageService<Etikette> {

	private static EtiketteStorageService m_Service;
	
	public static EtiketteStorageService getInstance() {
		if (m_Service == null) {
			m_Service = new EtiketteStorageService();
		}
		return m_Service;
	}

	@Override
	public Etikette createObject() {
		Etikette e = new Etikette();
		e.setID(UUID.randomUUID().toString());
		return e;
	}
}
