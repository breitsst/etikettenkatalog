package ch.luks.etiketten.nav.api;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import lotus.domino.AgentBase;
import ch.luks.etiketten.config.ConfigValue;
import ch.luks.etiketten.config.api.ConfigSessionFacade;
import ch.luks.etiketten.config.app.ConfigType;

public class NavSessionFacade extends AgentBase implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Abteilungen als Text
	public List<String> getAbteilungenText() {
		List<ConfigValue> lstCV = getAbteilungen();
		List<String> lstRC = new LinkedList<String>();
		for (ConfigValue cv : lstCV) {
			lstRC.add(cv.getValue());
		}
		return lstRC;
	}

	// Aktive Abteilungen f�r Navigation
	public List<ConfigValue> getActiveAbt() {
		return ConfigSessionFacade.get().getActiveConfigListe(ConfigType.ABTEILUNG.getKey());
	}

	public List<ConfigValue> getAbteilungen() {
		return ConfigSessionFacade.get().getConfigListe(ConfigType.ABTEILUNG.getKey());
	}
}
