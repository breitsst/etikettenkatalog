package ch.luks.etiketten.nav;

import java.io.Serializable;

public class NavLetter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String letter = "";

	// get und set
	public void setLetter(String letter) {
		this.letter = letter;
	}

	public String getLetter() {
		return letter;
	}
}
