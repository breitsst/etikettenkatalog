package ch.luks.etiketten.config;

public class StandardConfig {

	private String key = "";
	private String beschreibung = "";

	// Konstruktor
	public StandardConfig(String key, String beschreibung) {
		this.key = key;
		this.beschreibung = beschreibung;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
	public String getKey() {
		return key;
	}
	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}
	public String getBeschreibung() {
		return beschreibung;
	}
}
