package ch.luks.etiketten.config;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.openntf.xpt.core.dss.annotations.DominoEntity;
import org.openntf.xpt.core.dss.annotations.DominoStore;

@DominoStore(Form = "frmConfig", PrimaryKeyField = "ID", PrimaryFieldClass = String.class, View = "vwID")
public class Config implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Comparator wird f�r den Sort ben�tigt
	private static final Comparator<ConfigValue> COMPARATOR = new Comparator<ConfigValue>() {
		public int compare(ConfigValue arg0, ConfigValue arg1) {
			return Integer.valueOf(arg0.getSort()).compareTo(Integer.valueOf(arg1.getSort()));
		}
	};

	@DominoEntity(FieldName = "ID")
	private String m_ID;

	@DominoEntity(FieldName = "Beschreibung")
	private String m_Beschreibung;

	@DominoEntity(FieldName = "Key")
	private String m_Key;

	// MultiValue Field
	@DominoEntity(FieldName = "Values", embedded = true)
	private List<ConfigValue> m_Values = new LinkedList<ConfigValue>();

	private List<ConfigValue> m_ActiveValues = new LinkedList<ConfigValue>();

	public void addValue(String value) {
		ConfigValue cv = new ConfigValue();
		cv.setValue(value);
		cv.setAlias(getNewAlias());
		cv.setSort(m_Values.size());	// Sortindex
		m_Values.add(cv);
	}

	public void removeValue(ConfigValue cv) {
		int sortIndex = cv.getSort();
		m_Values.remove(cv);
		// Sortierung nachf�hren
		for (int i = sortIndex; i < m_Values.size(); i++) {
			ConfigValue ci = m_Values.get(i);
			ci.setSort(ci.getSort() - 1);
		}
	}
	
	private int getNewAlias() {
		int i = 0;
		for (ConfigValue cv: m_Values) {
			if (cv.getAlias() >= i) {
				i = cv.getAlias() + 1;
			}
		}
		return i;
	}

	public void activateValue(ConfigValue cv) {
		cv.setActive(true);
		m_ActiveValues.add(cv);
		Collections.sort(m_ActiveValues, COMPARATOR);
	}

	public void deactivateValue(ConfigValue cv) {
		cv.setActive(false);
		m_ActiveValues.remove(cv);
	}

	public void moveCvUp(ConfigValue cv) {
		int i = m_Values.indexOf(cv);
		if (i < m_Values.size() - 1) {
			switchCV(cv, m_Values.get(i + 1));
		}
	}

	public void moveCvDown(ConfigValue cv) {
		int i = m_Values.indexOf(cv);
		if (i > 0) {
			switchCV(cv, m_Values.get(i - 1));
		}
	}

	public void switchCV(ConfigValue cv1, ConfigValue cv2) {
		int s1 = cv1.getSort();
		int s2 = cv2.getSort();
		cv1.setSort(s2);
		cv2.setSort(s1);
		Collections.sort(m_Values, COMPARATOR);
		Collections.sort(m_ActiveValues, COMPARATOR);
	}

	public String getValueByAlias(String alias) {
		return getValueByAlias(Integer.parseInt(alias));
	}

	public String getValueByAlias(int alias) {
		for (ConfigValue cv: m_Values) {
			if (cv.getAlias() == alias) {
				return cv.getValue();
			}
		}
		return "";
	}

	public String getAliasByValue(String value) {
		for (ConfigValue cv: m_Values) {
			if (cv.getValue().equals(value)) {
				return "" + cv.getAlias();
			}
		}
		return "";
	}

	public List<String> getMultiValuesByAlias(List<String> alias) {
		List<String> lstReturn = new LinkedList<String>();
		for (String s: alias) {
			lstReturn.add(getValueByAlias(s));
		}
		return lstReturn;
	}

	// Liste "Value|Alias" ermitteln
	public List<String> getValueAliasPair() {
		List<String> lstReturn = new LinkedList<String>();
		String s = "";
		for (ConfigValue cv: m_Values) {
			s = cv.getValue() + "|" + cv.getAlias();
			lstReturn.add(s);
		}
		return lstReturn;
	}

	// get set
	public String getID() {
		return m_ID;
	}

	public void setID(String id) {
		m_ID = id;
	}

	public String getKey() {
		return m_Key;
	}

	public void setKey(String key) {
		m_Key = key;
	}

	public List<ConfigValue> getValues() {
		return m_Values;
	}

	public List<ConfigValue> getActiveValues() {
		return m_ActiveValues;
	}

	public void setValues(List<ConfigValue> values) {
		m_Values = values;
		if (m_ActiveValues == null) {
			m_ActiveValues = new LinkedList<ConfigValue>();
		}
		for (ConfigValue cv:m_Values) {
			if (cv.isActive()) {
				m_ActiveValues.add(cv);
			}
		}
		Collections.sort(m_Values, COMPARATOR);
		Collections.sort(m_ActiveValues, COMPARATOR);
	}

	public void setBeschreibung(String beschreibung) {
		m_Beschreibung = beschreibung;
	}

	public String getBeschreibung() {
		return m_Beschreibung;
	}

	public boolean isLowerBound(ConfigValue cv) {
		return m_Values.indexOf(cv) == 0;
	}

	public boolean isUpperBound(ConfigValue cv) {
		return m_Values.indexOf(cv) == m_Values.size() - 1;
	}

}
