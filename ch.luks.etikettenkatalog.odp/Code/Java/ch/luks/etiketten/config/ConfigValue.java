package ch.luks.etiketten.config;

import java.io.Serializable;

import org.openntf.xpt.core.dss.annotations.DominoEntity;
import org.openntf.xpt.core.dss.annotations.DominoStore;

@DominoStore(Form = "frmDummy", PrimaryFieldClass = String.class, PrimaryKeyField = "ID", View = "vwDummy")
public class ConfigValue implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@DominoEntity(FieldName = "Value")
	private String m_Value;
	
	@DominoEntity(FieldName = "Alias")
	private int m_Alias;
	
	@DominoEntity(FieldName = "Sort")
	private int m_Sort;
	
	@DominoEntity(FieldName = "Active")
	private boolean m_Active = false;

	public void switchActive() {
		if (m_Active) {
			m_Active = false;
		} else {
			m_Active = true;
		}
	}

	// get set
	public String getValue() {
		return m_Value;
	}

	public void setValue(String value) {
		m_Value = value;
	}

	public int getAlias() {
		return m_Alias;
	}

	public String getAliasTxt() {
		return "" + m_Alias;
	}

	public void setAlias(int alias) {
		m_Alias = alias;
	}

	public int getSort() {
		return m_Sort;
	}

	public void setSort(int sort) {
		m_Sort = sort;
	}

	public boolean isActive() {
		return m_Active;
	}

	public void setActive(boolean active) {
		m_Active = active;
	}
}
