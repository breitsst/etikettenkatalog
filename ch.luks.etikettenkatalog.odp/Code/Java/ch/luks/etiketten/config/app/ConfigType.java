package ch.luks.etiketten.config.app;

import java.util.List;

import ch.luks.etiketten.config.Config;
import ch.luks.etiketten.config.ConfigStorageService;
import ch.luks.etiketten.config.StandardConfig;
import ch.luks.etiketten.config.api.ConfigSessionFacade;

public class ConfigType {

	// Definition neuer Config-Dokumente mit Key und Beschreibung
	public static StandardConfig ETIKETTENTYP = new StandardConfig("EtikettenTyp", "Etiketten-Typ");
	public static StandardConfig ABTEILUNG = new StandardConfig("Abteilung", "Abteilungen");
	public static StandardConfig FARBENCODE = new StandardConfig("FarbenCode", "Farbencode");

	public static Config getConfig(StandardConfig sc) {
		// Pr�fen ob Config-Doc vorhanden ist
		List<Config> lstConfig = ConfigStorageService.getInstance().getObjectsByForeignId(sc.getKey(), ConfigSessionFacade.CONFIG_VIEW);
		Config c = null;
		if (lstConfig.size() > 0) {
			c = lstConfig.get(0);
		}

		// Wenn Config-Doc fehlt, erstellen
		if (c == null) {
			c = new Config();
			c.setKey(sc.getKey());
			c.setBeschreibung(sc.getBeschreibung());
			ConfigSessionFacade.get().saveConfig(c);
		}
		
		return c;
	}
}
