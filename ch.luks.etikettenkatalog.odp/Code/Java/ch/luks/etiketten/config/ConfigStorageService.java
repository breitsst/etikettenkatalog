package ch.luks.etiketten.config;

import java.util.UUID;
import org.openntf.xpt.core.dss.AbstractStorageService;

public class ConfigStorageService extends AbstractStorageService<Config> {

	private static ConfigStorageService m_Service;
	
	public static ConfigStorageService getInstance() {
		if (m_Service == null) {
			m_Service = new ConfigStorageService();
		}
		return m_Service;
	}

	@Override
	public Config createObject() {
		Config c = new Config();
		c.setID(UUID.randomUUID().toString());
		return c;
	}
}
