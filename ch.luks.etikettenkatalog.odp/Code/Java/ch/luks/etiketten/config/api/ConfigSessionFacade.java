package ch.luks.etiketten.config.api;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.faces.context.FacesContext;

import org.openntf.xpt.core.dss.DSSException;
import org.openntf.xpt.core.dss.SingleObjectStore;

import ch.luks.etiketten.config.Config;
import ch.luks.etiketten.config.ConfigStorageService;
import ch.luks.etiketten.config.ConfigValue;
import ch.luks.etiketten.config.StandardConfig;
import ch.luks.etiketten.config.app.ConfigType;

public class ConfigSessionFacade implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public final static String CONFIG_VIEW = "vwConfig";

	private static String BEAN_NAME = "configBean";

	private Config currentConfig = null;

	private String cID = "";

	public static ConfigSessionFacade get(FacesContext context) {
		ConfigSessionFacade bean = (ConfigSessionFacade) context.getApplication().getVariableResolver().resolveVariable(context, BEAN_NAME);
		return bean;
	}

	public static ConfigSessionFacade get() {
		return get(FacesContext.getCurrentInstance());
	}

	// Neue Config erstellen
	public Config createConfig() {
		return ConfigStorageService.getInstance().createObject();
	}

	// Config speichern
	public boolean saveConfig(Config config) {
		return ConfigStorageService.getInstance().save(config);
	}

	// Config l�schen
	public boolean deleteConfig(Config config) {
		try {
			return ConfigStorageService.getInstance().hardDelete(config, true);
		} catch (DSSException c) {
			// TODO Auto-generated catch block
			c.printStackTrace();
		}
		return false;
	}

	// Eine Config anhand einer ID lesen
	public Config getConfigByID(String id) {
		if (currentConfig == null || !id.equals(cID)) {
			System.out.println("ID: " + id + " --> wurde geholt!");
			cID = id;
			currentConfig = ConfigStorageService.getInstance().getById(id);
			System.out.println("config backend: " + currentConfig);
		}
		return currentConfig;
	}

	public List<Config> getAllConfigs() {
		return ConfigStorageService.getInstance().getAll(CONFIG_VIEW);
	}

	// Eine Config anhand des Keys lesen
	public Config getFirstConfigByKey(StandardConfig sc) {
		if (currentConfig == null || !sc.getKey().equals(cID)) {
			cID = sc.getKey();
			currentConfig = ConfigType.getConfig(sc);
		}
		return currentConfig;
	}

	public List<Config> getAllConfigsByKey(String key) {
		return ConfigStorageService.getInstance().getObjectsByForeignId(key, CONFIG_VIEW);
	}

	// ConfigListe lesen
	public List<ConfigValue> getConfigListe(String key) {
		List<Config> lstRC = ConfigStorageService.getInstance().getObjectsByForeignId(key, CONFIG_VIEW);
		List<ConfigValue> lstCV = new LinkedList<ConfigValue>();
		for (Config c : lstRC) {
			lstCV.addAll(c.getValues());
		}
		return lstCV;
	}

	public List<ConfigValue> getActiveConfigListe(String key) {
		List<Config> lstRC = ConfigStorageService.getInstance().getObjectsByForeignId(key, CONFIG_VIEW);
		List<ConfigValue> lstCV = new LinkedList<ConfigValue>();
		for (Config c : lstRC) {
			List<ConfigValue> lstAV = c.getActiveValues();
			if (lstAV != null) {
				lstCV.addAll(c.getActiveValues());
			}
		}
		return lstCV;
	}

	public SingleObjectStore<Config> getSOS() {
		return new SingleObjectStore<Config>(ConfigStorageService.getInstance());
	}
}
